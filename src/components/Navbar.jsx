import HomeIcon from "@mui/icons-material/Home";
import { Box } from "@mui/material";
import CreateBoard from "./CreateBoard";
import { Link } from "react-router-dom";
import '../Navbar.css'
const NavBar = () => {
  return (
    
      <Box
        sx={{
          display: "flex",
          backgroundColor: "white",
          boxShadow: "2px 2px 5px rgba(5, 5, 5, 0.413);",
          position:"fixed",
          overflow:"hidden",
          width:'100%',
        
        }}
      >
        <div className="heading"></div>
        <div style={{display:"flex", alignItems:"center"}}>
        <div style={{marginRight:"20px"}}>
        <Link to={"/"} style={{ color: "black" }}>
          <HomeIcon />
        </Link>
        </div>
        <CreateBoard />
        </div>
      </Box>
    
  );
};

export default NavBar;
