import DeleteIcon from "@mui/icons-material/Delete";
import { IconButton } from "@mui/material";
import { deleteItem } from "../api";

const DeleteButton = ({ type, id, checkId, onDelete }) => {
  const handleDelete = async () => {
    try {
      await deleteItem(type, id, checkId, onDelete);
    } catch (error) {
      console.error("Error deleting item:", error);
    }
  };

  return (
    <IconButton sx={{padding:0}} onClick={handleDelete} color="secondary" aria-label="delete">
      <DeleteIcon />
    </IconButton>
  );
};

export default DeleteButton;
