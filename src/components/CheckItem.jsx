import { useEffect, useState } from "react";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

import CreateCheckItem from "./CreateCheckItem";
import DeleteButton from "./DeleteButton";
import PropTypes from "prop-types";
import { getCheckItemsForChecklist, updateCheckItemState } from "../api";

const CheckItem = ({ checklistId, cardId }) => {
  const [checkItemsData, setCheckItemsData] = useState([]);

  const getData = async () => {
   
    try {
      const checkItemsData = await getCheckItemsForChecklist(checklistId);
      setCheckItemsData(checkItemsData);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  const newCheckItemGenerated = (data) => {
    setCheckItemsData([...checkItemsData, data]);
  };

  const handleDelete = (checkItemId) => {
    setCheckItemsData((prevList) =>
      prevList.filter((item) => item.id !== checkItemId)
    );
  };

  const handleCheckboxChange = async (checkItemId, cardId, currentState) => {
   
    try {
      await updateCheckItemState(cardId, checkItemId, currentState, setCheckItemsData);
    } catch (error) {
      console.error('Error updating check item state:', error);
    }
  };
  return (
    <>
      {checkItemsData.map((checkItem) => {
        return (
          <>
            <div  style={{ display: "flex" }}>
              <FormGroup sx={{ marginRight: "auto" }}>
                <FormControlLabel
                  key={checkItem.id}
                  control={
                    <Checkbox
                      defaultChecked={
                        checkItem.state === "complete" ? true : false
                      }
                      onChange={() => {
                        handleCheckboxChange(
                          checkItem.id,
                          cardId,
                          checkItem.state
                        );
                      }}
                    />
                  }
                  label={checkItem.name}
                />
              </FormGroup>
              <DeleteButton
                type="checkItem"
                id={checklistId}
                checkId={checkItem.id}
                onDelete={handleDelete}
              />
            </div>
          </>
        );
      })}

      <CreateCheckItem
        checklistId={checklistId}
        newCheckItemGenerated={newCheckItemGenerated}
      />
    </>
  );
};

CheckItem.propTypes ={
  checklistId: PropTypes.any.isRequired,
  cardId: PropTypes.any.isRequired,
}
export default CheckItem;
