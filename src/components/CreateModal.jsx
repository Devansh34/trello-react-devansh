import { Box, Button, Modal, TextField, Typography } from "@mui/material";
import { useState } from "react";
import PropTypes from "prop-types";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const CreateModal = ({ addBoardData}) => {
  const [open, setOpen] = useState(false);
  const [name, setName] = useState("");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handleChange = (e) => {
    setName(e.target.value);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    addBoardData(name);
    setName("");
    handleClose();
  };
  return (
    <div>
      <Button onClick={handleOpen} style={{padding:0}}>Create New Board</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          {/* <Typography id="modal-modal-title" variant="h6" component="h2" sx={{marginRight:1}}>
            New Board Name
          </Typography> */}
          <form onSubmit={handleSubmit} style={{paddingBottom:"1vh"}}>
            <label>
              <TextField value={name} label="Write Board Name" type="text" onChange={(e) => handleChange(e)} sx={{height:'0.8rem'}}/>
            </label>
            
              <Button type="submit" variant="contained" sx={{marginTop:"0.5vh",marginLeft:"0.5vw",height:"6vh"}}>Create Board</Button>
            
          </form>
        </Box>
      </Modal>
    </div>
  );
};

CreateModal.propTypes = {
  addBoardData: PropTypes.func.isRequired,

};
export default CreateModal;
