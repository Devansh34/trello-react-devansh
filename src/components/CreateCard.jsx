import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  TextField,
  Typography,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useState } from "react";
import PropTypes from "prop-types";
import { createNewCard } from "../api";

const CreateCard = ({ listId, cardCreated }) => {
  const [newCardName, setNewCardName] = useState("");
  const handleClick = async () => {
    try {
      await createNewCard(listId, newCardName, cardCreated);
      setNewCardName("");
    } catch (error) {
      console.error('Error creating card:', error);
    }
  };
  return (
    <>
      <Accordion sx={{ marginTop: 2 }}>
        <AccordionSummary
          expandIcon={<AddIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Add a card</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 1},
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="outlined-basic"
              label="Enter a title for this card..."
              variant="outlined"
              value={newCardName}
              onChange={(e) => setNewCardName(e.target.value)}
            />
            <Button
              variant="contained"
              onClick={(e) => {
                e.preventDefault();
                handleClick();
              }}
            >
              Add card
            </Button>
          </Box>
        </AccordionDetails>
      </Accordion>
    </>
  );
};
CreateCard.propTypes = {
  listId: PropTypes.string.isRequired,
  cardCreated: PropTypes.func.isRequired,
};

export default CreateCard;
