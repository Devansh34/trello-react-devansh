import { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import CreateCard from "./CreateCard";
import ChecklistModal from "./ChecklistModal";
import DeleteButton from "./DeleteButton";
import { getCardsForList } from "../api";

const Cards = ({ listId }) => {
  const [cards, setCards] = useState([]);

  const Item = styled(Paper)(({ theme }) => ({
    color: theme.palette.text.secondary,
    padding: "10px",
  }));

  const getData = async () => {
    try {
      const cardsData = await getCardsForList(listId);
      setCards(cardsData);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    getData();
  }, []);
  const newCardGenerated = (newdata) => {
    setCards([...cards, newdata]);
  };

  const handleDelete = (id) => {
    setCards((prevList) => prevList.filter((item) => item.id !== id));
  };

  return (
    <>
      <Box sx={{ width: "100%" }}>
        <Stack spacing={2} sx={{ maxHeight: "55vh", overflowY: "auto"}}>
          {cards.map((c) => {
            return (
              <>
                <Item
                  key={c.id}
                  sx={{ display: "flex", justifyContent: "space-between" ,backgroundColor:"#f5f5f5"}}
                >
                  {c.name}
                  <ChecklistModal cardId={c.id} />

                  <DeleteButton type="card" id={c.id} onDelete={handleDelete} />
                </Item>
              </>
            );
          })}
          <CreateCard listId={listId} cardCreated={newCardGenerated} />
        </Stack>
      </Box>
    </>
  );
};

Cards.propTypes = {
  listId: PropTypes.string.isRequired,
};

export default Cards;
