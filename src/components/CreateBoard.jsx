
import CreateModal from "./CreateModal";
import { useNavigate } from "react-router-dom";
import { createNewBoard } from "../api";

const CreateBoard = () => {
  const navigate = useNavigate();
  const addBoardData = async (boardName) => {
  
    try {
      const data = await createNewBoard(boardName);

      // Assuming you're using Reach Router to navigate
      navigate(`/boards/${data.id}`);
    } catch (error) {
      console.error('Error creating board:', error);
    }
  };

  return (
    <div>
      <CreateModal addBoardData={addBoardData} />
    </div>
  );
};

export default CreateBoard;
