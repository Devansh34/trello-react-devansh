import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  TextField,
  Typography,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { useState } from "react";
import { createNewList } from "../api";
import PropTypes from "prop-types";

const CreateList = ({ id, setListItem }) => {
  const [newListName, setNewListName] = useState("");
  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

  
    try {
      const newList = await createNewList(id, newListName);

      setListItem((prevList) => [...prevList, newList]);
      setNewListName("");
      setExpanded(false);
    } catch (error) {
      console.error('Error creating new list:', error);
    }

  };

  return (
    <Box sx={{ width: "100%", minWidth: 280,maxWidth:360 ,paddingRight:5 }}>
      <div>
        <Accordion
          key="accordionkey"
          expanded={expanded === "panel1"}
          onChange={handleChange("panel1")}
          sx={{
            boxShadow: "2px 2px 5px rgba(5, 5, 5, 0.413);",
            borderRadius: "8px",
            backgroundColor: "white",
          }}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography sx={{ width: "33%", flexShrink: 0 }}>
              Add New List
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <form onSubmit={handleSubmit}>
              <TextField
                label="List Name"
                variant="outlined"
                value={newListName}
                onChange={(e) => setNewListName(e.target.value)}
                sx={{ marginBottom: 1 }}
              />
              <Button type="submit" variant="contained" color="primary">
                Add List
              </Button>
            </form>
          </AccordionDetails>
        </Accordion>
      </div>
    </Box>
  );
};
CreateList.propTypes = {
  id: PropTypes.string.isRequired,
  setListItem: PropTypes.any.isRequired,
};
export default CreateList;
