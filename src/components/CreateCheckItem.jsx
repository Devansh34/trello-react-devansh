import { Box, Button, TextField } from "@mui/material";

import { useState } from "react";
import PropTypes from "prop-types";
import { createNewCheckItem } from "../api";

const CreateCheckItem = ({ checklistId, newCheckItemGenerated }) => {
  const [newCheckItemName, setNewCheckItemName] = useState("");
  const handleClick = async () => {
    try {
      await createNewCheckItem(checklistId, newCheckItemName, newCheckItemGenerated);
      setNewCheckItemName("");
    } catch (error) {
      console.error('Error creating check item:', error);
    }
  };
  return (
    <>
      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "15ch" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          label="Enter a title for this checkitem..."
          variant="outlined"
          value={newCheckItemName}
          onChange={(e) => setNewCheckItemName(e.target.value)}
        />
        <Button
          variant="contained"
          onClick={(e) => {
            e.preventDefault();
            handleClick();
          }}
        >
          Add item
        </Button>
      </Box>
    </>
  );
};

CreateCheckItem.propTypes={
  checklistId: PropTypes.any.isRequired,
  newCheckItemGenerated:PropTypes.any.isRequired,
}

export default CreateCheckItem;
