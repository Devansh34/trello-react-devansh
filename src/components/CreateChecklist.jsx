import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  TextField,
  Typography,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useState } from "react";

import { createNewChecklist } from "../api";

const CreateChecklist = ({ cardId, checklistCreated }) => {
  const [newChecklistName, setNewChecklistName] = useState("");
  const handleClick = async () => {
    try {
      await createNewChecklist(cardId, newChecklistName, checklistCreated);
      setNewChecklistName("");
    } catch (error) {
      console.error("Error creating checklist:", error);
    }
  };
  return (
    <>
      <Accordion sx={{ marginTop: 2 }}>
        <AccordionSummary
          expandIcon={<AddIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Add a Checklist</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 1, width: "15ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="outlined-basic"
              label="Enter a title for this cardlist..."
              variant="outlined"
              value={newChecklistName}
              onChange={(e) => setNewChecklistName(e.target.value)}
            />
            <Button
              variant="contained"
              onClick={(e) => {
                e.preventDefault();
                handleClick();
              }}
            >
              Add checklist
            </Button>
          </Box>
        </AccordionDetails>
      </Accordion>
    </>
  );
};

export default CreateChecklist;
