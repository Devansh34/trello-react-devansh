import { Box, Button, Modal } from "@mui/material";
// import Stack from "@mui/joy/Stack";
// import LinearProgress from "@mui/joy/LinearProgress";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import CreateChecklist from "./CreateChecklist";
import CheckItem from "./CheckItem";
import DeleteButton from "./DeleteButton";
import { getChecklistsForCard } from "../api";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "fit-content",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  display: "flex",
};

const ChecklistModal = ({ cardId }) => {
  const [open, setOpen] = useState(false);
  const [checklist, setChecklist] = useState([]);
  // let count = 0;
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const getData = async () => {
    try {
      const checklistsData = await getChecklistsForCard(cardId);
      setChecklist(checklistsData);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const newChecklistGenerated = (data) => {
    setChecklist([...checklist, data]);
  };

  const handleDelete = (id) => {
    setChecklist((prevList) => prevList.filter((item) => item.id !== id));
  };

  return (
    <div>
      <Button
        onClick={handleOpen}
        style={{
          padding: 0,
          backgroundColor: "transparent",
          // border: "none",
          width: "14rem",
          height: "4vh",
          position: "absolute",
          // top: "0",
          left: "0",
        }}
      ></Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div style={{ display: "flex", flexDirection: "column" }}>
            {checklist.map((checklistItem) => (
              <>
                <div key={checklistItem.id} style={{ marginBottom: 8 }}>
                  <Accordion>
                    <div style={{ display: "flex", flexDirection: "column" }}>
                      <AccordionSummary
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                        sx={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <Typography sx={{ marginRight: "auto" }}>
                          {checklistItem.name}
                        </Typography>

                        <DeleteButton
                          type="checklist"
                          id={checklistItem.id}
                          onDelete={handleDelete}
                        />
                      </AccordionSummary>
                      {/* {console.log(checklistItem.checkItems.length)} */}
                      {/* {console.log(checklistItem.checkItems)}
                      {console.log(
                        (count / checklistItem.checkItems.length) * 100
                      )}
                      {checklistItem.checkItems.map((c) => {
                        if (c.state === "complete") {
                          count = count + 1;
                        }
                      })}
                        
                      
                      <Stack spacing={2} sx={{ flex: 1 }}>
                        <LinearProgress
                          determinate
                          value={
                            (count / checklistItem.checkItems.length) * 100
                          }
                        />
                      </Stack> */}
                    </div>
                    <AccordionDetails>
                      <CheckItem
                        checklistId={checklistItem.id}
                        cardId={cardId}
                      />
                    </AccordionDetails>
                  </Accordion>
                </div>
              </>
            ))}
            <CreateChecklist
              cardId={cardId}
              checklistCreated={newChecklistGenerated}
            />
          </div>
        </Box>
      </Modal>
    </div>
  );
};

ChecklistModal.propTypes = {
  cardId: PropTypes.any.isRequired,
};

export default ChecklistModal;
