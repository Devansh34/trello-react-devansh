import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import NavBar from "./components/Navbar";
import Boards from "./pages/Boards";
import Lists from "./pages/Lists";
import Error from "./pages/Error";
import { BoardProvider } from "./components/BoardProvider";
// import { BoardProvider } from "./components/background";

function App() {
  return (
    <>
      <BrowserRouter>
       <NavBar />
       <BoardProvider>
        <Routes>
          <Route path="/" element={<Boards />} />
          <Route path="/boards" element={<Boards />} />
          <Route path="/boards/:id" element={<Lists />} />
          <Route path="/*" element={<Error />} />
        </Routes>
        </BoardProvider>
        
      </BrowserRouter>
    </>
  );
}

export default App;
