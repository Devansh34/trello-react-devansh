import { Box, List, ListItem, ListItemText } from "@mui/material";
// import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CreateList from "../components/CreateList";
import Cards from "../components/Cards";
import DeleteButton from "../components/DeleteButton";
import { TailSpin } from "react-loader-spinner";
import { getListsForBoard } from "../api";
import { useNavigate } from "react-router-dom";
import { useBoardContext } from "../components/BoardProvider";

const Lists = () => {
  const { id } = useParams();
  const [listItem, setListItem] = useState([]);
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();
  const { backgroundImageObject, backgroundColorObject } = useBoardContext();
  const fetchData = async () => {
    try {
      const listsData = await getListsForBoard(id);
      setListItem(listsData);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
      navigate("/Error");
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = (id) => {
    setListItem((prevList) => prevList.filter((item) => item.id !== id));
  };
  
  return (
    <>
      <div
        style={{
          backgroundColor: backgroundColorObject[id]
            ? backgroundColorObject[id]
            : "#0079BF",
          backgroundImage: `url(${backgroundImageObject[id]})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          height: "100vh",
          overflowX: "auto",
        }}
      >
        {!loading ? (
          <Box
            className="boardContainer"
            sx={{
              display: "flex",
              paddingBlock: 15,
              paddingLeft: 5,
              alignItems: "start",
              // backgroundColor:backgroundColorObject[id]?backgroundColorObject[id]:'#0079BF',
              // backgroundImage:`url(${backgroundImageObject[id]})`,
              // backgroundSize:"cover",
              // backgroundPosition:"center",
              // minHeight:"70vh",
              // overflowX:'auto',
            }}
          >
            {listItem.map((m) => (
              <Box
                key={m.id}
                sx={{
                  width: "100%",
                  minWidth: 280,
                  maxWidth: 360,
                  marginRight: 5,
                  marginBottom: 1,
                  boxShadow: "2px 2px 5px rgba(5, 5, 5, 0.413);",
                  borderRadius: "8px",
                  paddingInline: "10px",
                  backgroundColor: "white",
                }}
              >
                <nav aria-label="main mailbox folders">
                  <List>
                    <ListItem disablePadding>
                      <ListItemText primary={m.name} />

                      <DeleteButton
                        type="list"
                        id={m.id}
                        onDelete={handleDelete}
                      />
                    </ListItem>
                    <Cards listId={m.id} />
                  </List>
                </nav>
              </Box>
            ))}
            <CreateList id={id} setListItem={setListItem} />
          </Box>
        ) : (
          <div
            style={{
              width: "100vw",
              height: "100vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <TailSpin
              visible={true}
              height="80"
              width="80"
              color="blue"
              ariaLabel="tail-spin-loading"
              radius="1"
              wrapperStyle={{}}
              wrapperClass=""
            />
          </div>
        )}
      </div>
    </>
  );
};

export default Lists;
