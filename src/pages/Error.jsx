

const Error = () => {
  return (
    <div style={{paddingBlock:"20vh",textAlign:"center"}}>
      <h1 > OOPS </h1>
      <h2>Error! This page Does Not Exist</h2>
    </div>
  )
}

export default Error;
