import { useEffect, useState } from "react";
import { Box } from "@mui/material";
import CardContent from "@mui/material/CardContent";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import {TailSpin} from 'react-loader-spinner'
import {getBoards} from '../api'
import CreateBoard from "../components/CreateBoard";
import { Link } from "react-router-dom";
import { BoardProvider } from "../components/BoardProvider";

const Boards = () => {
  const [boards, setBoards] = useState([]);
  
  
  const fetchData = async () => {
    try {
      const boardsData = await getBoards();
      setBoards(boardsData);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };
  useEffect(() => {

    fetchData();
    
  }, []);

  return (<>
    {boards.length?(
    <Box
      className="boardContainer"
      sx={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
        paddingInline: 15,
        paddingBlock: 12,
        gap: 12,
      }}
    >
      <BoardProvider boardData={boards}/>
      {boards.map((b) => {
        return (
          <Link
            key={b.id}
            to={{
              pathname: `/boards/${b.id}`
            }}
            style={{ textDecoration: "none" }}
          >
            <Card
              key={b.id}
              sx={{
                minWidth: 200,
                minHeight: "fit-content",
                marginBottom: 5,
                backgroundColor: b.prefs.backgroundColor,
                backgroundImage: `url(${b.prefs.backgroundImage})`,
                backgroundSize: "cover",
              }}
            >
              <CardContent
                sx={{
                  backgroundColor: "rgba(0,0,0,0.35)",
                  width: "100%",
                  minHeight: "10vh",
                }}
              >
                <Typography variant="p" gutterBottom sx={{ color: "white" }}>
                  {b.name}
                </Typography>
              </CardContent>
            </Card>
          </Link>
        );
      })}
      <Card
        sx={{ minWidth: 200, minHeight: 100, marginRight: 5, marginBottom: 5 }}
      >
        <CardContent>
          <CreateBoard />
        </CardContent>
      </Card>
    </Box>):<div style={{width:"100vw", height:"100vh",display:"flex",justifyContent:"center",alignItems:"center"}}><TailSpin
    visible={true}
    height="80"
    width="80"
    color="blue"
    ariaLabel="tail-spin-loading"
    radius="1"
    wrapperStyle={{}}
    wrapperClass=""
    
    /></div>}
    </>
  );
};

export default Boards;
