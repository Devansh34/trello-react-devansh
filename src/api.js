import axios from "axios";

axios.defaults.baseURL = "https://api.trello.com/1/";
axios.defaults.params = {
  key: "9925e3c167c105d47e25f8473628687b",
  token:
    "ATTA576ea99022560a7821f3b011d0e329870c20557f1c4f2d147f2f254b1ef652ddE9B723D9",
};

const getBoards = async () => {
  try {
    const response = await axios.get("members/me/boards");
    return response.data;
  } catch (error) {
    console.error("Error getting boards:", error.message);
    throw error;
  }
};

const createNewBoard = async (boardName) => {
    try {
      const response = await axios.post('boards', {
        name: boardName,
      });
  
      if (response.status === 200) {
        const data = response.data;
        return data;
      }
    } catch (error) {
      console.error('Error creating board:', error);
      throw error;
    }
  };

const getListsForBoard = async (boardId) => {
  try {
    const response = await axios.get(`boards/${boardId}/lists`);
    return response.data;
  } catch (error) {
    console.error("Error getting lists for board:", error.message);
    throw error;
  }
};
const createNewList = async (idBoard, newListName) => {
  try {
    const response = await axios.post("lists", {
      name: newListName,
      idBoard,
    });

    return response.data;
  } catch (error) {
    console.error("Error creating new list:", error.message);
    throw error;
  }
};

const getCardsForList = async (listId) => {
  try {
    const response = await axios.get(`lists/${listId}/cards`);
    return response.data;
  } catch (error) {
    console.error("Error getting cards for list:", error.message);
    throw error;
  }
};

const createNewCard = async (listId, newCardName, cardCreated) => {
  try {
    const response = await axios.post("cards", {
      idList: listId,
      name: newCardName,
    });

    if (response.status === 200) {
      const data = response.data;
      cardCreated(data);
    }
  } catch (error) {
    console.error("Error creating card:", error);
    throw error;
  }
};
const getChecklistsForCard = async (cardId) => {
  try {
    const response = await axios.get(`cards/${cardId}/checklists`);
    return response.data;
  } catch (error) {
    console.error("Error getting checklists for card:", error.message);
    throw error;
  }
};
const createNewChecklist = async (
  cardId,
  newChecklistName,
  checklistCreated
) => {
  try {
    const response = await axios.post("checklists", {
      idCard: cardId,
      name: newChecklistName,
    });

    if (response.status === 200) {
      const data = response.data;
      checklistCreated(data);
    }
  } catch (error) {
    console.error("Error creating checklist:", error);
    throw error;
  }
};

const getCheckItemsForChecklist = async (checklistId) => {
  try {
    const response = await axios.get(`checklists/${checklistId}/checkItems`);
    return response.data;
  } catch (error) {
    console.error("Error getting check items for checklist:", error.message);
    throw error;
  }
};

const createNewCheckItem = async (
  checklistId,
  newCheckItemName,
  newCheckItemGenerated
) => {
  try {
    const response = await axios.post(`checklists/${checklistId}/checkItems`, {
      name: newCheckItemName,
    });

    if (response.status === 200) {
      const data = response.data;
      newCheckItemGenerated(data);
    }
  } catch (error) {
    console.error("Error creating check item:", error);
    throw error;
  }
};

const updateCheckItemState = async (
  cardId,
  checkItemId,
  currentState,
  setCheckItemsData
) => {
  try {
    const newState = currentState === "complete" ? "incomplete" : "complete";

    const response = await axios.put(
      `cards/${cardId}/checkItem/${checkItemId}`,
      {
        state: newState,
      }
    );

    setCheckItemsData((prevList) =>
      prevList.map((item) =>
        item.id === checkItemId ? { ...item, state: newState } : item
      )
    );

    return response;
  } catch (error) {
    console.error("Error updating check item state:", error);
    throw error;
  }
};

const deleteItem = async (type, id, checkId, onDelete) => {
  try {
    let url = "";
    let method = "DELETE";
    let itemId = "";

    if (type === "card") {
      url = `cards/${id}`;
      itemId = id;
    } else if (type === "checklist") {
      url = `checklists/${id}`;
      itemId = id;
    } else if (type === "checkItem") {
      url = `checklists/${id}/checkItems/${checkId}`;
      itemId = checkId;
    } else if (type === "list") {
      // Use PUT request to close the list
      url = `lists/${id}/closed?value=true`;
      method = "PUT";
      itemId = id;
    } else {
      console.error("Invalid type");
      return;
    }

    await axios({ method, url });
    onDelete(itemId);
  } catch (error) {
    console.error("Error deleting item:", error);
    throw error;
  }
};

export {
  getBoards,
  createNewBoard,
  getListsForBoard,
  createNewList,
  getCardsForList,
  createNewCard,
  getChecklistsForCard,
  createNewChecklist,
  getCheckItemsForChecklist,
  createNewCheckItem,
  updateCheckItemState,
  deleteItem,
};
